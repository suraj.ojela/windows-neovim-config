" Vim plug
" Specify a directory for plugins
call plug#begin('~/AppData/Local/nvim/plugged')

	Plug 'easymotion/vim-easymotion' " Improved motions
	Plug 'ervandew/supertab' " Insert completion with tab
	Plug 'jiangmiao/auto-pairs' " Insert/delete/etc brackets/quotes in pairs
	Plug 'kien/rainbow_parentheses.vim' " Add colour to all brackets
	Plug 'kshenoy/vim-signature' " Marks
	Plug 'neomake/neomake' " Linting and make
	Plug 'scrooloose/nerdcommenter' " Commenting
	Plug 'tpope/vim-repeat' " Add support for repeating plugin commands
	Plug 'tpope/vim-surround' " Surround with brackets etc.
	Plug 'tpope/vim-unimpaired' " Various keybindings
	Plug 'vim-airline/vim-airline' " Statusline
	Plug 'vim-scripts/SearchComplete' " Tab completion inside searches
	Plug 'https://github.com/tyru/open-browser.vim' " Open urls

" Initialize plugin system
call plug#end()

function SpellCheck()
	setlocal spell! spelllang=en_gb
endfunction

filetype plugin on " Load plugins for filetype
filetype indent on " Load indent file for filetype
syntax enable " Syntax highlighting
set relativenumber
set number
set splitright
set cursorline
set cursorcolumn
set lazyredraw
set complete=.,w,k,b,u,t,i,kspell
set clipboard+=unnamedplus " Use clipboard for all operations
set hlsearch
set ignorecase
set smartcase
nnoremap <silent> <leader>8 :call SpellCheck()<CR>
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
inoremap <C-x>l <C-x><C-l>
inoremap <C-x>n <C-x><C-n>
inoremap <C-x>k <C-x><C-k>
inoremap <C-x>t <C-x><C-t>
inoremap <C-x>i <C-x><C-i>
inoremap <C-x>] <C-x><C-]>
inoremap <C-x>f <C-x><C-f>
inoremap <C-x>d <C-x><C-d>
inoremap <C-x>v <C-x><C-v>
inoremap <C-x>u <C-x><C-u>
inoremap <C-x>o <C-x><C-o>
inoremap jj <Esc>

autocmd BufWrite * silent! %s/\s\+$// " Remove trailing white space

set omnifunc=syntaxcomplete#Complete " Turn on omni completion

call neomake#configure#automake('nrwi', 100)

" Execute 'lnoremap x X' and 'lnoremap X x' for each letter a-z. (Caps lock)
for c in range(char2nr('A'), char2nr('Z'))
	execute 'lnoremap ' . nr2char(c+32) . ' ' . nr2char(c)
	execute 'lnoremap ' . nr2char(c) . ' ' . nr2char(c+32)
endfor

" Kill the capslock when leaving insert mode.
autocmd InsertLeave * set iminsert=0

" Vim-repeat
silent! call repeat#set("\<Plug>[<Space>", v:count)
silent! call repeat#set("\<Plug>]<Space>", v:count)

" Netrw
let g:netrw_banner = 0        " disable annoying banner
let g:netrw_browse_split = 4  " open in prior window
let g:netrw_altv = 1          " open splits to the right
let g:netrw_liststyle = 3     " tree view

" NERD Commenter
filetype plugin on
let g:NERDSpaceDelims = 1 " Add spaces after comment delimiters
let g:NERDDefaultAlign = 'left' " Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDCommentEmptyLines = 1 " Allow commenting and inverting empty lines
let g:NERDTrimTrailingWhitespace = 1 " Enable trimming of trailing whitespace when uncommenting

" Rainbow Parentheses
let g:rbpt_colorpairs = [
			\ ['brown',       'RoyalBlue3'],
			\ ['Darkblue',    'SeaGreen3'],
			\ ['darkgray',    'DarkOrchid3'],
			\ ['darkgreen',   'firebrick3'],
			\ ['darkcyan',    'RoyalBlue3'],
			\ ['darkred',     'SeaGreen3'],
			\ ['darkmagenta', 'DarkOrchid3'],
			\ ['brown',       'firebrick3'],
			\ ['gray',        'RoyalBlue3'],
			\ ['darkmagenta', 'DarkOrchid3'],
			\ ['Darkblue',    'firebrick3'],
			\ ['darkgreen',   'RoyalBlue3'],
			\ ['darkcyan',    'SeaGreen3'],
			\ ['darkred',     'DarkOrchid3'],
			\ ['red',         'firebrick3'],
			\ ]
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" Open urls

let g:netrw_nogx = 1 " disable netrw's gx mapping.
nmap gx <Plug>(openbrowser-smart-search)
vmap gx <Plug>(openbrowser-smart-search)
